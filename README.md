Introduction
===

In the training Jupyter Notebook, a fully connected neural network
is trained to learn a function based on inputs and outputs,
without being told what the function actually is.


Live demo
===

A live demo link is provided at this repository's description.
The live demo generates random inputs and assesses the model's accuracy.


Training
===

The function to learn is:
- `1` if `x is divisible by 2`
- `1` if `x is divisible by 3` and `x is not divisible by 7`
- `0` otherwise

This particular function is mostly arbitrary.
It has been chosen because it is relatively simple,
and there would be approximately equal `1`s and `0`s
for random inputs. This prevents the class imbalance problem.

Random, unsigned integers are used as inputs.
The integers are broken down into its bits as the input's features.
For example, `29` in base 10 is `11101` in base 2,
and `[1, 1, 1, 0, 1]` is fed into the model.

The inputs in this example have `BIT_LENGTH` bits.
This means the inputs ranges from `0` to `2^BIT_LENGTH - 1`.

In the training phase, at most 75% of all possible inputs have
been used to demonstrate the learned generalization by model.


Results
===

The learned model's layer weights has a size of 2244 bytes and
the model's accuracy is approximately 85%. Considering that at
most 75% of all possible inputs have been used for training,
the learned generalization is good.

Supposing that we allow inputs to have up to 16 bits (that is,
`BIT_LENGTH = 16`), there would be `2^16 = 65536` possible inputs.
If one bit was used to store a true label for each possible input,
then it would take `ceil(65536 bits * 1 byte/8 bits) = 8192 bytes`
to get 100% accuracy by naïvely storing the results.
The learned model only takes approximately 27.4% of this size.
