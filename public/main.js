const BIT_LENGTH = 16;

function toBitArray(number) {
  const result = new Array(BIT_LENGTH);

  for (let i = 0; i < BIT_LENGTH; i++) {
    const bitIndex = BIT_LENGTH - 1 - i;

    const mask = 1 << bitIndex;

    result[i] = (number & mask) > 0;
  }

  return result;
}

async function load() {
  const model = await tf.loadLayersModel("model_js/model.json");
  return model;
}

function predictRandom(model) {
  const isModuloN = (x, n) => x % n == 0;

  const numInputs = document.getElementById("quantity").value;

  if (numInputs < 1) {
    return;
  }

  const resultDiv = document.getElementById("results-all");
  resultDiv.innerHTML = "";

  // Create an array containing integers from 0 to (2^BIT_LENGTH - 1)
  const maxVal = Math.pow(2, BIT_LENGTH);
  const xs_orig = Array.from({ length: numInputs }, () =>
    Math.floor(Math.random() * maxVal)
  );
  let xs = [...xs_orig];

  // Convert each integer to its features (bits)
  xs = xs.map((x) => toBitArray(x));
  xs = tf.tensor(xs);

  model.then((model) => {
    const predictions = model.predict(xs).dataSync();

    const resultsTable = document.createElement("table");

    const tr = resultsTable.insertRow();

    let th = document.createElement("th");
    th.appendChild(document.createTextNode("Input (x)"));
    tr.appendChild(th);

    th = document.createElement("th");
    th.appendChild(document.createTextNode("Model prediction (y_pred)"));
    tr.appendChild(th);

    th = document.createElement("th");
    th.appendChild(document.createTextNode("Is correct? (y == y_pred)"));
    tr.appendChild(th);

    let numCorrect = 0;

    predictions.forEach((prediction, i) => {
      // Threshold the prediction so that if it is closer to 1,
      // then it is considered true, and if it is closer to 0,
      // then it is considered false
      const result = prediction > 0.5;

      const predictionText = result ? "True" : "False";

      const x = xs_orig[i];

      const isResultCorrect =
        (isModuloN(x, 2) || (isModuloN(x, 3) && !isModuloN(x, 7))) == result;

      const resultCorrectText = isResultCorrect ? "Yes" : "No";

      if (isResultCorrect) {
        numCorrect++;
      }

      const tr = resultsTable.insertRow();

      let td = tr.insertCell();
      td.appendChild(document.createTextNode(x));

      td = tr.insertCell();
      td.appendChild(document.createTextNode(predictionText));

      td = tr.insertCell();
      td.appendChild(document.createTextNode(resultCorrectText));

      if (!isResultCorrect) {
        tr.classList.add("incorrect-prediction");
      }
    });

    const resultsDiv = document.getElementById("results-all");

    resultsDiv.innerHTML = `
        Random inputs used: ${numInputs}<br />
        <br />
        Correct predictions: ${numCorrect}<br />
        Incorrect predictions: ${numInputs - numCorrect}<br />
        <br />
        Accuracy: ${((numCorrect / numInputs) * 100).toFixed(2)}%<br />
      `;
    resultsDiv.appendChild(resultsTable);
  });
}

const model = load();
